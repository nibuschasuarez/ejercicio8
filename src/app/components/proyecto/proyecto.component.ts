import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-proyecto',
  templateUrl: './proyecto.component.html',
  styleUrls: ['./proyecto.component.css']
})
export class ProyectoComponent implements OnInit {
   valor1:number = 0;
   a: number= 1;
   mensaje:boolean=false;


  suma(){
    if(this.valor1<50){
      this.valor1 =this.valor1 + this.a;
      this.mensaje= false;
    }else{
      this.mensaje = true;
    }
  }

  resta(){
    if(this.valor1>0){
      this.valor1 = this.valor1- this.a;
      this.mensaje = false;
    }else{
      this.mensaje = true;
    }

  }

 

  constructor() { 
    this.suma
    this.resta
  }

  ngOnInit(): void {
  }

}
